export const state = () => ({
    categories: null,
    isGettingCategories: false,
});

const MUTATIONS = {
    SET_CATEGORIES: 'SET_CATEGORIES',
    SET_LOADING: 'SET_LOADING',
};

export const getters = {
    getCategories(state) {
        return state.categories;
    },

    getLoadingState(state) {
        return state.isGettingCategories;
    },
};

export const mutations = {
    [MUTATIONS.SET_CATEGORIES](state, categories) {
        state.categories = categories;
    },

    [MUTATIONS.SET_LOADING](state, loading) {
        state.isGettingCategories = loading;
    },
};

export const actions = {
    async fetchAll({ commit }) {
        try {
            commit(MUTATIONS.SET_LOADING, true);
            const { data: allCategories } = await this.$api.categories.getAll();
            const categories = allCategories.map((category) => ({ value: category.id, label: category.name }));
            commit(MUTATIONS.SET_CATEGORIES, categories);
        } catch (error) {
            console.log(error);
        } finally {
            commit(MUTATIONS.SET_LOADING, false);
        }
    },
};
