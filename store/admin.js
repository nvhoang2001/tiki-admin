const defaultAvatar = 'https://salt.tikicdn.com/desktop/img/avatar.png';

export const state = () => ({
    id: '',
    username: '',
    nick_name: '',
    avatar_url: '',
    birthday: '',
    gender: '',
    address: '',
    phone_number: '',
    email: '',
});

export const getters = {
    getUserId(state) {
        return state.id;
    },

    getName(state) {
        return state.username;
    },

    getNickName(state) {
        return state.nick_name;
    },

    getAvatar(state) {
        return state.avatar_url || defaultAvatar;
    },

    getInfo(state) {
        return state;
    },

    getPhoneNumber(state) {
        return state.phone_number;
    },
};

const MUTATIONS = {
    UPDATE_INFO: 'UPDATE_INFO',
};

export const mutations = {
    [MUTATIONS.UPDATE_INFO](state, user) {
        state.id = user.id;
        state.username = user.username;
        state.nick_name = user.nick_name;
        state.avatar_url = user.avatar_url;
        state.birthday = user.birthday;
        state.gender = String(user.gender);
        state.address = user.address;
        state.phone_number = user.phone_number;
        state.email = user.email;
    },
};

const ACTIONS = {
    UPDATE_INFO: 'UPDATE_INFO',
    GET_INFO: 'GET_INFO',
};

export const actions = {
    async [ACTIONS.UPDATE_INFO](context, user) {
        let isFailed = null;
        try {
            const { user: updatedUser } = await this.$api.user.updateUser(
                context.state.id,
                user,
            );
            context.commit(MUTATIONS.UPDATE_INFO, updatedUser);
        } catch (error) {
            console.log(error);
            isFailed = error;
        }

        return isFailed;
    },

    async [ACTIONS.GET_INFO](context) {
        let isFailed = null;
        try {
            const user = await this.$api.user.getInfo();
            context.commit(MUTATIONS.UPDATE_INFO, user);
        } catch (error) {
            console.log(error);
            isFailed = error;
        }
        return isFailed;
    },
};
