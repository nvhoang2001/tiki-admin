export const STATE = {
    PENDING: 'pending',
    REJECTED: 'rejected',
    APPROVED: 'approved',
};

export const STATE_VI = {
    pending: 'Chờ duyệt',
    rejected: 'Từ chối',
};
