require('dotenv').config();

const isProduction = process.env.NODE_ENV === 'production';

export default {
    dev: !isProduction,
    // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
    ssr: false,

    // Global page headers: https://go.nuxtjs.dev/config-head
    head: {
        title: 'Tiki Admin | Quản trị Tiki',
        htmlAttrs: {
            lang: 'vi',
        },
        meta: [
            { charset: 'utf-8' },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1',
            },
            { hid: 'description', name: 'description', content: '' },
            { name: 'format-detection', content: 'telephone=no' },
        ],

        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            {
                rel: 'icon', type: 'image/png', sizes: '32x32', href: '/favicon.png',
            },
            {
                rel: 'icon', type: 'image/png', sizes: '16x16', href: '/favicon-16x16.png',
            },
            { rel: 'apple-touch-icon', sizes: '180x180', href: '/apple-touch-icon.png' },
            { rel: 'manifest', href: '/site.webmanifest' },

        ],
    },

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: ['@/assets/main.scss', 'ant-design-vue/dist/antd.css', '@fortawesome/fontawesome-free/css/all.css'],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        '@/plugins/antd-ui',
        '@/plugins/filters',
        '@/plugins/api',
        '@/plugins/global-components',
        '@/plugins/axios',
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [
        // https://go.nuxtjs.dev/eslint
        '@nuxtjs/eslint-module',
        '@nuxt/postcss8',
        '@nuxtjs/fontawesome',
        '@nuxtjs/tailwindcss',
    ],

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        '@nuxtjs/auth-next',
    ],

    router: {
        middleware: ['auth'],
    },

    auth: {
        redirect: {
            login: '/dang-nhap',
        },
        strategies: {
            local: {
                token: {
                    property: 'data.token',
                    global: true,
                    maxAge: 'data.expired_time',
                },
                endpoints: {
                    login: { url: '/api/auth/admin/login', method: 'post' },
                    user: false,
                    logout: { url: '/api/auth/logout', method: 'post' },
                },
            },
        },
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {
        // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
        baseURL: process.env.API_HOST,
        credentials: true,
    },

    server: {
        host: process.env.HOST || 'localhost',
        port: process.env.PORT || '3000',
    },

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        postcss: {
            plugins: {
                tailwindcss: 'tailwind.config.js',
                autoprefixer: {},
                ...(process.env.APP_ENV === 'production'
                    ? { cssnano: {} }
                    : {}),
            },
        },
        loaders: {},
        babel: {
            plugins: [
                [
                    'import',
                    {
                        libraryName: 'ant-design-vue',
                        libraryDirectory: 'es',
                        style: true,
                    },
                    'ant-design-vue',
                ],
            ],
        },
    },
    env: {
        apiHost: process.env.API_HOST || 'localhost',
        apiPath: process.env.API_PATH || '/api',
        appUrl: process.env.APP_URL || 'http://localhost:3000',
    },
};
