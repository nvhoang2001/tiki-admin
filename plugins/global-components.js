import Vue from 'vue';
import Card from '~/components/shared/Card.vue';
import Search from '~/components/shared/Search.vue';
import ThePagination from '~/components/shared/ThePagination.vue';

const components = [
    {
        name: 'card', component: Card,
    },
    {
        name: 'search', component: Search,
    },
    {
        name: 'the-pagination', component: ThePagination,
    },
];

components.forEach(({ name, component }) => {
    Vue.component(name, component);
});
