import Common from '~/api/common';
import User from '~/api/user';
import Auth from '~/api/auth';
import Categories from '~/api/categories';
import Products from '~/api/products';
import Order from '~/api/order';
import Dashboard from '~/api/dashboard';

export default (context, inject) => {
    // Initialize API factories
    const factories = {
        common: Common(context.$axios),
        user: User(context.$axios),
        auth: Auth(context.$axios),
        categories: Categories(context.$axios),
        products: Products(context.$axios),
        order: Order(context.$axios),
        dashboard: Dashboard(context.$axios),
    };

    // Inject $api
    inject('api', factories);
};
