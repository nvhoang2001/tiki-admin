import Cookies from 'js-cookie';

export default ({ $axios }) => {
    function requestHandler(config) {
        if (
            (config.method === 'post'
                || config.method === 'put'
                || config.method === 'delete')
            && !Cookies.get('XSRF-TOKEN')
        ) {
            return setCSRFToken().then((_) => config);
        }
        return config;
    }

    function setCSRFToken() {
        return $axios.get('/sanctum/csrf-cookie');
    }

    function responseHandler(response) {
        if (response.data.status && response.data.status >= 400) {
            return Promise.reject(response.data);
        }
        return response;
    }
    $axios.onRequest(requestHandler);
    $axios.onResponse(responseHandler);
};
