// Default config here: https://github.com/tailwindlabs/tailwindcss/blob/master/stubs/defaultConfig.stub.js

module.exports = {
    content: [],
    theme: {
        extend: {
            spacing: {
                21: '5.25rem',
                22: '5.5rem',
            },
        },
    },
    plugins: [],
}
