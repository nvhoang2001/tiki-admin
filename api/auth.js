export default (axios) => ({
    forgetPassword: (data) => axios.post('/api/auth/forgotPassword', data).then((_) => _.data),
    changePassword: (data) => axios.post('/api/auth/change-password', data).then((_) => _.data),
});
