export default (axios) => ({
    getAll: () => axios.get('/api/orders/statistic').then((_) => _.data),
});
