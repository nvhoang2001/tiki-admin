export default (axios) => ({
    upload: (data) => axios.post('/api/upload', data).then((_) => _.data),
});
