export default (axios) => ({
    getAll: (params) => axios.get('/api/products', { params }).then((_) => _.data),
    getProduct: (id) => axios.get(`/api/products/${id}`).then((_) => _.data),
    create: (form) => axios.post('/api/products', form).then((_) => _.data),
    update: (id, form) => axios.post(`/api/products/${id}`, form).then((_) => _.data),
    delete: (id) => axios.delete(`/api/products/${id}`).then((_) => _.data),
});
