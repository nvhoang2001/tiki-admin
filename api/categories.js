export default (axios) => ({
    getAll: (params) => axios.get('/api/categories', { params }).then((_) => _.data),
    getSingle: (id) => axios.get(`/api/categories/${id}`).then((_) => _.data),
    create: (form) => axios.post('/api/categories', form).then((_) => _.data),
    edit: (id, form) => axios.post(`/api/categories/${id}`, form).then((_) => _.data),
    delete: (id) => axios.delete(`/api/categories/${id}`).then((_) => _.data),
});
