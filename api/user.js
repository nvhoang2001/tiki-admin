export default (axios) => ({
    getAll: (params) => axios.get('/api/user', { params }).then((_) => _.data),
    getUser: (id) => axios.get(`/api/user/${id}`).then((_) => _.data),
    getInfo: () => axios.get('/api/me').then((_) => _.data),
    updateUser: (id, user) => axios.put(`/api/user/update/${id}`, user).then((_) => _.data),
});
