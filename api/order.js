export default (axios) => ({
    getAll: (params) => axios.get('/api/orders', { params }).then((_) => _.data),
    getOrder: (id) => axios.get(`/api/orders/${id}`).then((_) => _.data),
    update: (id, form) => axios.put(`/api/orders/${id}`, form).then((_) => _.data),
});
